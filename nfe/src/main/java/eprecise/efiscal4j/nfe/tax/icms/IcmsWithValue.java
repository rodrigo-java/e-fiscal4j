
package eprecise.efiscal4j.nfe.tax.icms;

public interface IcmsWithValue {

    String getIcmsValue();
}
