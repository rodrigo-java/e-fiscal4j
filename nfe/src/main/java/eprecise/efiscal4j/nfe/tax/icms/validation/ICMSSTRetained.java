
package eprecise.efiscal4j.nfe.tax.icms.validation;



@ICMSSTRetainedValidation
public interface ICMSSTRetained {

	public String getBcRetainedValueST();

	public String getIcmsRetainedValueST();
}
